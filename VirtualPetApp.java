import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		
		//Creating a new Snake array
		Snake[] snakeDen = new Snake[1];
		
		Scanner reader = new Scanner(System.in);
		//for loop, user input for every field
		for(int i =0; i < snakeDen.length; i++){
			
			System.out.println("Enter your snake's color");
			String snakeColor = reader.nextLine();
			
			System.out.println("Your snake's length in cm");
			int snakeLength = Integer.parseInt(reader.nextLine());
			
			System.out.println("Is your snake striped? Answer true or false");
			boolean snakeIsStriped = Boolean.parseBoolean(reader.nextLine());
			
			snakeDen[i] = new Snake(snakeColor, snakeLength, snakeIsStriped);
		}
		// prints values of the first snake
		System.out.print("\n");
		System.out.println(snakeDen[0].getColor());
		System.out.println(snakeDen[0].getLength());
		System.out.println(snakeDen[0].getIsStriped());
		
		// cahnges the color of the last snake
		System.out.print("\n");
		System.out.println("Your snake shed its skin, what color is your snake?");
		snakeDen[snakeDen.length-1].setColor(reader.nextLine());
		
		// print values of the first snake after changing the color of the last Snake
		System.out.print("\n");
		System.out.println(snakeDen[0].getColor());
		System.out.println(snakeDen[0].getLength());
		System.out.println(snakeDen[0].getIsStriped());
	}
}