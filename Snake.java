public class Snake{
	//setting 3 fields
	private String color;
	private int length; //in centimeters
	private boolean striped;
	
	//get methods
	public String getColor(){
		return this.color;
	}
	public int getLength(){
		return this.length;
	}
	public boolean getIsStriped(){
		return this.striped;
	}
	
	//set methods
	public void setColor(String color){
		this.color = color;
	}
	
	//constructor
	public Snake(String color, int length, boolean striped){
		this.color = color;
		this.length = length;
		this.striped = striped;
	}
	
	
	public int grow(int length){
		if(length < 100){
			System.out.println(length + " centimeters is too small you can't have babies, wait 1 year");
			length += 30;
		}else{
			System.out.println(length + " centimeters is enough, good luck");
		}
		return length;
	}
	//ideas: add length to it, if length > 2, have babies becomes true, else you must grow
	public void makeBabies(boolean stripes){
		if(stripes){
			System.out.println("You will have striped babies");
		}else{
			System.out.println("Youre babies won't be striped");
		}
	}
	
}